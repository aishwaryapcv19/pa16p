#include<stdio.h>
int swap(int *x,int *y)
{
int t;
t=*x;
*x=*y;
*y=t;
}
int main()
{
int num1,num2;
printf("Enter two numbers");
scanf("%d%d",&num1,&num2);
printf("Before swapping num1=%d, num2=%d",num1,num2);
swap(&num1,&num2);
printf("After swapping num1=%d,num2=%d",num1,num2);
return 0;
}